<?php

use craft\elements\Entry;
use craft\elements\GlobalSet;
use craft\helpers\UrlHelper;



return [
    'endpoints' => [
        'api/global' => function() {
            
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            
            return [
                'elementType' => GlobalSet::Class,
                'criteria' => ['handle' => 'global', 'site' => $langHandle],
                'one' => true,
                'transformer' => function(GlobalSet $globalSet) {

                    $sections = [];
                    foreach ($globalSet->contentFields as $section) {
                        $sections[$section->textKey] = $section->textValue;
                    }

                    return [
                        'blocks' => $sections
                    ];
                }
            ];
        },

        'api/nav' => function() {

            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');

            return [
                'elementType' => GlobalSet::Class,
                'criteria' => ['handle' => 'navigation', 'site' => $langHandle],
                'one' => true,
                'transformer' => function(GlobalSet $globalSet) {

                    $sections = [];
                    foreach ($globalSet->navigation as $section) {
                        $page = $section->navPage[0];

                        $subpages = [];
                        if($page->slug == 'resources'){
                            $subpages[] = [
                                'title' => $page->linksTitle,
                                'slug' => 'links'
                            ];
                            $subpages[] = [
                                'title' => $page->glossaryTitle,
                                'slug' => 'glossary'
                            ];
                            $subpages[] = [
                                'title' => $page->referencesTitle,
                                'slug' => 'references'
                            ];
                        } else {
                            foreach ($section->navSubPages as $subpageEntry) {
                                $subpages[] = [
                                    'title' => $subpageEntry->title,
                                    'slug' => $subpageEntry->slug
                                ];
                            }
                        }

                        $sections[] = [
                            'title' => $page->title,
                            'slug' => $page->slug,
                            'subpages' => $subpages
                        ];
                    }

                    return [
                        'blocks' => $sections
                    ];
                }
            ];


        },

        'api/page/content/<slug>' => function($slug) {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => $slug, 'site' => $langHandle],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'description' => $entry->description,
                    ];
                }
            ];
        },

        'api/page/home' => function() {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => 'home', 'site' => $langHandle],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'intro' => $entry->homeIntroCopy,
                        'copy' => $entry->homeMainCopy,
                    ];
                }
            ];
        },
        'api/page/project' => function() {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => 'project', 'site' => $langHandle],
                'one'=>true,
                'transformer' => function(Entry $entry) {

                    $sections = [];
                    foreach ($entry->pageSections as $section) {
                        $item = [
                            'title' => $section->title,
                            'slug' => $section->slug,
                            'snippet' => $section->intro,
                            'content' => $section->description,

                        ];
                        $sections[] = $item;
                    }
                    return [
                        'title' => $entry->title,
                        'intro' => $entry->pageIntro,
                        'sections' => $sections
                    ];
                }
            ];
        },
        'api/page/why' => function() {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => 'why', 'site' => $langHandle],
                'one'=>true,
                'transformer' => function(Entry $entry) {

                    $sections = [];
                    foreach ($entry->pageSections as $section) {
                        $item = [
                            'title' => $section->title,
                            'slug' => $section->slug,
                            'snippet' => $section->intro,
                            'content' => $section->description,

                        ];
                        $sections[] = $item;
                    }
                    return [
                        'title' => $entry->title,
                        'intro' => $entry->pageIntro,
                        'sections' => $sections
                    ];
                }
            ];
        },
        'api/page/resources' => function() {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'one' => true,
                'criteria' => ['slug' => 'resources', 'site' => $langHandle],
                'transformer' => function(Entry $entry) {
                    $links = [];
                    foreach ($entry->resourceLinks as $link) {
                        $item = [
                            'title' => $link->itemTitle,
                            'url' => $link->itemUrl
                        ];
                        $links[] = $item;
                    }
                    $lists = [];
                    foreach ($entry->referenceList as $list) {
                        $item = [
                            'title' => $list->sectionTitle,
                            'content' => $list->sectionContent
                        ];
                        $lists[] = $item;
                    }
                    $articles = [];
                    foreach ($entry->newArticles as $article) {
                        $item = [
                            'title' => $article->title,
                            'intro' => $article->intro,
                            'slug' => $article->slug,
                            'description' => $article->description,
                        ];
                        $articles[] = $item;
                    }


                    return [
                        'title' => $entry->title,
                        'intro' => $entry->intro,
                        'newsTitle' => $entry->newsTitle,
                        'newsIntro' => $entry->newsIntro,
                        'linksTitle' => $entry->linksTitle,
                        'linksIntro' => $entry->resourceLinksIntro,
                        'glossaryTitle' => $entry->glossaryTitle,
                        'glossaryIntro' => $entry->glossaryIntro,
                        'referencesTitle' => $entry->referencesTitle,
                        'referencesIntro' => $entry->referenceListIntro,
                        'news' => $articles,
                        'links' => $links,
                        'references' => $lists,
                        'glossary' => $entry->glossary

                    ];
                }
            ];
        },
        'api/page/families' => function() {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => 'families', 'site' => $langHandle],
                'transformer' => function(Entry $entry) {
                    $tabs = [];;
                    foreach ($entry->familyTabs as $tab) {
                        $item = [
                            'title' => $tab->tabTitle,
                            'content' => $tab->tabContent,
                        ];
                        $tabs[] = $item;
                    }
                    return [
                        'title' => $entry->title,
                        'intro' => $entry->familiesIntro,
                        'copy' => $entry->familiesCopy,
                        'tabs' => $tabs
                    ];
                }
            ];
        },
        'api/families' => function() {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['section' => 'family', 'site' => $langHandle],
                'transformer' => function(Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'slug' => $entry->slug,
                    ];
                }
            ];
        },
        'api/family/<slug>' => function($slug) {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => $slug, 'site' => $langHandle],
                'one' => true,
                'transformer' => function(Entry $entry) {

                    // Create an array of all the photo URLs
                    $photos = [];
                    foreach ($entry->speciesPhotos->all() as $photo) {
                        $photoAsset = $photo->photoAsset->one();
                        $arr = [
                            'caption' => $photo->photoCaption,
                            'author' => $photo->photoAuthor,
                            // 'photo' => $photo->photoAsset->one()->getUrl(),
                            // 'thumb' => $photo->photoAsset->one()->getUrl('imageThumbnail'),
                        ];
                        if($photoAsset){
                         $arr['photo'] = $photoAsset->getUrl();
                         $arr['thumb'] = $photoAsset->getUrl('imageThumbnail');
                        }
                        $photos[] = $arr;

                    }

                    $bodyBlocks = [];
                    foreach ($entry->genusKeyChart as $block) {
                       if($block->type->handle != 'sectionSubtitle'){
                         $item = [
                             'group' => $block->keyTextGroup,
                             'description' => $block->keyTextDescription
                         ];
                       }
                        if($block->type->handle == 'groupKeyText') {
                            $item['type'] = 'group';
                            $item['subgroup'] = $block->keyTextSubgroup;
                        }else if($block->type->handle == 'genusKeyText'){
                            $genusEntry = $block->genusKey->one();
                            $item['type'] = $genusEntry->type->handle;
                            $item['genus'] = [
                                'slug' => $genusEntry->slug,
                                'name' => $genusEntry->shortName
                            ];
                        }else if($block->type->handle == 'sectionSubtitle'){
                            $item['type'] = 'subtitle';
                            $item['subtitle'] = $block->subtitle;
                        }

                        $bodyBlocks[] = $item;
                    }
                    return [
                        'title' => $entry->title,
                        'name' => $entry->nameText,
                        'description' => $entry->description,
                        'shortDescription' => $entry->shortDescription,
                        'photos' => $photos,
                        'shortName' => $entry->shortName,
                        'references' => $entry->references,
                        'notes' => $entry->notes,
                        'url' => $entry->url,
                        'chart' => $bodyBlocks
                    ];
                }
            ];
        },
        'api/subfamily/<slug>' => function($slug) {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => $slug, 'site' => $langHandle],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    $family = $entry->family->first();

                    $bodyBlocks = [];
                    foreach ($entry->genusKeyChart as $block) {
                        $item = [];
                        if($block->type->handle == 'groupKeyText') {
                         $item = [
                             'group' => $block->keyTextGroup,
                             'description' => $block->keyTextDescription
                         ];
                            $item['type'] = 'group';
                            $item['subgroup'] = $block->keyTextSubgroup;
                        }else if($block->type->handle == 'genusKeyText'){
                         $item = [
                             'group' => $block->keyTextGroup,
                             'description' => $block->keyTextDescription
                         ];
                            $item['type'] = 'genus';
                            $genusEntry = $block->genusKey->one();
                            $item['genus'] = [
                                'slug' => $genusEntry->slug,
                                'name' => $genusEntry->shortName
                            ];
                        }else if($block->type->handle == 'sectionSubtitle'){
                            $item['type'] = 'subtitle';
                            $item['subtitle'] = $block->subtitle;
                        }else{
                          $item['type'] = 'genus';
                         // $speciesEntry = $block->speciesKey->one();
                         $item['genus'] = [
                             'name' => $block->genusKey,
                         ];
                        }

                        $bodyBlocks[] = $item;
                    }

                    // Create an array of all the photo URLs
                    $photos = [];
                    foreach ($entry->speciesPhotos->all() as $photo) {
                        $photoAsset = $photo->photoAsset->one();
                        $arr = [
                            'caption' => $photo->photoCaption,
                            'author' => $photo->photoAuthor,
                            // 'photo' => $photo->photoAsset->one()->getUrl(),
                            // 'thumb' => $photo->photoAsset->one()->getUrl('imageThumbnail'),
                        ];
                        if($photoAsset){
                         $arr['photo'] = $photoAsset->getUrl();
                         $arr['thumb'] = $photoAsset->getUrl('imageThumbnail');
                        }
                        $photos[] = $arr;

                    }

                    return [
                        'title' => $entry->title,
                        'slug' => $entry->slug,
                        'description' => $entry->description,
                        'shortDescription' => $entry->shortDescription,
                        'shortName' => $entry->shortName,
                        'references' => $entry->references,
                        'photos' => $photos,
                        'family' => [
                            'name' => $family->shortName,
                            'slug' => $family->slug,
                        ],
                        'chart' => $bodyBlocks

                    ];
                }
            ];
        },
        'api/genus/<slug>' => function($slug) {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => $slug, 'site' => $langHandle],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    $family = $entry->family->first();
                    $subfamily = null;
                    $subfamilyData = [];
                    if($family->type->handle == 'subfamily') {
                     $subfamily = $entry->family->first();
                     $subfamilyData = [
                         'name' => $subfamily->shortName,
                         'slug' => $subfamily->slug,
                     ];
                     $family = $subfamily->family->first();

                    }
                    $familyData = [
                        'name' => $family->shortName,
                        'slug' => $family->slug,
                    ];

                    // Create an array of all the photo URLs
                    $photos = [];
                    foreach ($entry->speciesPhotos->all() as $photo) {
                        $photoAsset = $photo->photoAsset->one();
                        $arr = [
                            'caption' => $photo->photoCaption,
                            'author' => $photo->photoAuthor,
                            // 'photo' => $photo->photoAsset->one()->getUrl(),
                            // 'thumb' => $photo->photoAsset->one()->getUrl('imageThumbnail'),
                        ];
                        if($photoAsset){
                         $arr['photo'] = $photoAsset->getUrl();
                         $arr['thumb'] = $photoAsset->getUrl('imageThumbnail');
                        }
                        $photos[] = $arr;

                    }

                    $bodyBlocks = [];
                    foreach ($entry->speciesKeyChart as $block) {
                        $item = [
                            'group' => $block->keyTextGroup,
                            'description' => $block->keyTextDescription
                        ];
                        if($block->type->handle == 'groupKeyText') {
                            $item['type'] = 'group';
                            $item['subgroup'] = $block->keyTextSubgroup;
                        }else if($block->type->handle == 'speciesKeyText'){
                            $item['type'] = 'species';
                            $speciesEntry = $block->speciesKey->one();
                            $item['species'] = [
                                'slug' => $speciesEntry->slug,
                                'name' => $speciesEntry->shortName
                            ];
                        }else{
                          $item['type'] = 'species';
                         // $speciesEntry = $block->speciesKey->one();
                         $item['species'] = [
                             'name' => $block->speciesKey,
                         ];
                        }

                        $bodyBlocks[] = $item;
                    }

                    return [
                        'test' => $family->type->handle,
                        'photos' => $photos,
                        'title' => $entry->title,
                        'slug' => $entry->slug,
                        'description' => $entry->description,
                        'shortDescription' => $entry->shortDescription,
                        'shortName' => $entry->shortName,
                        'references' => $entry->references,
                        'subfamily' => $subfamilyData,
                        'family' => $familyData,
                        'chart' => $bodyBlocks

                    ];
                }
            ];
        },

        'api/search' => function () {

            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            $query = Craft::$app->request->getQueryParam('q', '');

            return [
              'elementType' => Entry::class,
              'paginate' => true,
              'elementsPerPage' => 20,
              'criteria' => [
                'section' => ['family', 'genus', 'subfamily', 'species'],
                'search' => 'title:*'.$query.'* OR description:*'.$query.'*',
                'site' => $langHandle,
                'orderBy' => 'score',
              ],
              'pageParam' => 'page',
              'transformer' => function(Entry $entry) {
                return [
                    'section' => $entry->section->handle,
                    'title' => $entry->title,
                    'slug' => $entry->slug,
                    'shortName' => $entry->shortName,
                    'description' => $entry->description,
                ];
              }
            ];

            // (craft()->request->getParam('q')) ? 'title:'.'*'.craft()->request->getParam('q').'*'.' OR ' . 'blogSummary:'.'*'.craft()->request->getParam('q').'*' : ''

        },

        'api/search/map' => function () {

            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            $ne = explode(',', Craft::$app->request->getQueryParam('ne', '0,0'));
            $sw = explode(',', Craft::$app->request->getQueryParam('sw', '0,0'));
            
            $inputBoundsNorth = $ne[0];
            $inputBoundsEast = $ne[1];
            $inputBoundsSouth = $sw[0];
            $inputBoundsWest = $sw[1];

            return [
                'elementType' => Entry::class,
                'paginate' => true,
                'elementsPerPage' => 20,
                'pageParam' => 'page',
                'criteria' => [
                    'boundsNorth' => '>= ' . $inputBoundsSouth,
                    'boundsSouth' => '<= ' . $inputBoundsNorth,
                    'boundsWest' => '<= ' . $inputBoundsEast,
                    'boundsEast' => '>= ' . $inputBoundsWest,
                    'section' => 'species', 
                    'site' => $langHandle
                ],
                'transformer' => function(Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'description' => $entry->description,
                        'boundsNorth' => $entry->boundsNorth,
                        'boundsWest' => $entry->boundsWest,
                        'boundsSouth' => $entry->boundsSouth,
                        'boundsEast' => $entry->boundsEast,
                    ];
                }
            ];

        },

        'api/species/<slug>' => function($slug) {
            $langHandle = Craft::$app->request->getQueryParam('lang', 'en');
            return [
                'elementType' => Entry::class,
                'criteria' => ['slug' => $slug, 'site' => $langHandle],
                'one' => true,
                'transformer' => function(Entry $entry) {

                    $genus = $entry->genus->first();
                    $family = $genus->family->first();
                    $subfamily = null;
                    $subfamilyData = [];
                    if($family->type->handle == 'subfamily') {
                     $subfamily = $genus->family->first();
                     $subfamilyData = [
                         'name' => $subfamily->shortName,
                         'slug' => $subfamily->slug,
                     ];
                     $family = $subfamily->family->first();

                    }
                    $familyData = [
                        'name' => $family->shortName,
                        'slug' => $family->slug,
                    ];

                    // Create an array of all the photo URLs
                    $photos = [];
                    foreach ($entry->speciesPhotos->all() as $photo) {
                        $photoAsset = $photo->photoAsset->one();
                        $arr = [
                            'caption' => $photo->photoCaption,
                            'author' => $photo->photoAuthor,
                            // 'photo' => $photo->photoAsset->one()->getUrl(),
                            // 'thumb' => $photo->photoAsset->one()->getUrl('imageThumbnail'),
                        ];
                        if($photoAsset){
                         $arr['photo'] = $photoAsset->getUrl();
                         $arr['thumb'] = $photoAsset->getUrl('imageThumbnail');
                        }
                        $photos[] = $arr;

                    }

                    $kml = $entry->speciesKml->one();

                    return [
                        'title' => $entry->title,
                        'name' => $entry->nameText,
                        'url' => $entry->url,
                        'genus' => [
                            'name' => $genus->shortName,
                            'slug' => $genus->slug,
                        ],
                        'subfamily' => $subfamilyData,
                        'family' => $familyData,
                        'otherNames' => $entry->otherNames,
                        'shortName' => $entry->shortName,
                        'description' => $entry->description,
                        'shortDescription' => $entry->shortDescription,
                        'flowering' => $entry->flowering,
                        'fruiting' => $entry->fruiting,
                        'habitat' => $entry->habitat,
                        'distribution' => $entry->distribution,
                        'observations' => $entry->observations,
                        'conservation' => $entry->conservation,
                        'photos' => $photos,
                        'kml' => $kml ? $kml->url : null,
                    ];
                },
            ];
        },
    ]
];
