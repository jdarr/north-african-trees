import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/',
      component: Home,
    },
    {
      path: '/project',
      component: () => import(/* webpackChunkName: "project-detail" */ './views/Project.vue'),
    },
    {
      path: '/project/:slug',
      component: () => import(/* webpackChunkName: "content-block" */ './views/ContentBlock.vue'),
    },
    {
      path: '/why/:slug',
      component: () => import(/* webpackChunkName: "content-block" */ './views/ContentBlock.vue'),
    },
    {
      path: '/article/:slug',
      component: () => import(/* webpackChunkName: "content-block" */ './views/ContentBlock.vue'),
    },
    {
      path: '/why',
      component: () => import(/* webpackChunkName: "why" */ './views/Why.vue'),
    },
    {
      path: '/resources',
      component: () => import(/* webpackChunkName: "Resources" */ './views/Resources.vue'),
    },
    {
      path: '/resource/:slug',
      component: () => import(/* webpackChunkName: "Resource" */ './views/Resource.vue'),
    },
    {
      path: '/families',
      component: () => import(/* webpackChunkName: "Resources" */ './views/Families.vue'),
    },
    {
      path: '/family/:id',
      component: () => import(/* webpackChunkName: "Resources" */ './views/Family.vue'),
    },
    {
      path: '/subfamily/:id',
      component: () => import(/* webpackChunkName: "Resources" */ './views/Subfamily.vue'),
    },
    {
      path: '/genus/:id',
      component: () => import(/* webpackChunkName: "Genus" */ './views/Genus.vue'),
    },
    {
      path: '/species/:id',
      component: () => import(/* webpackChunkName: "Species" */ './views/Species.vue'),
    },
    {
      path: '/map/:ne/:sw',
      component: () => import(/* webpackChunkName: "Species" */ './views/MapResults.vue'),
    },
    {
      path: '/search/:query',
      component: () => import(/* webpackChunkName: "Species" */ './views/SearchResults.vue'),
    },
    {
      path: '/contact',
      component: () => import(/* webpackChunkName: "Species" */ './views/Contact.vue'),
    },

  ],
})
