import axios from 'axios'

class API {
  constructor () {
    this.api = '/api/'
  }

  makeRequest (path, local = 'en', params = '') {
    return axios.get(this.api + path + "?lang=" + local + params)
  }

  getBasePath() {
    return this.api
  }

  getEmailPath () {
    return 'http://backend-trees-shrubs.jareddev.local/'
  }
}

export default new API()
