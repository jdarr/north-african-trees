import Vue from 'vue'
import Vuex from 'vuex'
import Cookie from 'js-cookie'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isMobile: false,
    mobileNav: false,
    // photoModal:false,
    formModal: false,
    navData: false,
    globalData: false,
    locale: Cookie.get('lang') ? Cookie.get('lang') : 'en',
  },
  mutations: {
    GLOBAL_DATA (state, data) {
      state.globalData = data
    },
    NAV_DATA (state, data) {
      state.navData = data
    },
    MOBILE_NAV_OPEN (state, isOpen) {
      state.mobileNav = isOpen
    },
    IS_MOBILE (state, isMobile) {
      state.isMobile = isMobile
    },
    FORM_MODAL_OPEN (state, isOpen) {
      state.formModal = isOpen
    },
    SET_LOCALE (state, locale) {
      state.locale = locale
    },
  },
  actions: {

    setLocale ({ commit }, locale) {
      commit('SET_LOCALE', locale)
      Cookie.set('lang', locale)
    },
    setMobileNav ({ commit }, { isOpen }) {
      commit('MOBILE_NAV_OPEN', isOpen)
    },
    setGlobalData ({ commit }, data) {
      console.log(data)
      commit('GLOBAL_DATA', data)
    },
    setNavData ({ commit }, { blocks }) {
      console.log(blocks)
      commit('NAV_DATA', blocks)
    },
    setIsMobile ({ commit }, { isMobile }) {
      commit('IS_MOBILE', isMobile)
    },
    setFormModal ({ commit, state }, { isOpen }) {
      if (state.isMobile) {
        commit('MOBILE_NAV_OPEN', false)
      }
      commit('FORM_MODAL_OPEN', isOpen)
    },
  },
})
