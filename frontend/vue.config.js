const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}

const port = process.env.DEV_PORT || 3000
const https = process.env.HTTPS_OVERRIDE === 'true'

module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? `/static` : '/',
  outputDir: process.env.NODE_ENV === 'production' ? `dist/static` : 'dist',
  indexPath: process.env.NODE_ENV === 'production' ? path.join(__dirname, `dist/index.html`) : 'index.html',
  configureWebpack: {
    resolve: {
      alias: {
        '%': resolve('public'),
      },
    },
  },
  chainWebpack: config => {
    // helps fix safari cache bug for hot reloading
    if (process.env.NODE_ENV === 'development') {
      config
        .output
        .filename('[name].[hash].js')
        .end()
    }

    config.plugins.delete('prefetch')
  },
  devServer: {
    https,
    port,
    allowedHosts: [
      '.14four.com',
      '.jareddev.local',
      '.jaredlaptop.localhost',
    ],
  },
}