# KML Bounds plugin for Craft CMS 3.x

Takes a KML Asset and parses the cordinates to calculate the bounds of the file

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /kml-bounds

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for KML Bounds.

## KML Bounds Overview

-Insert text here-

## Configuring KML Bounds

-Insert text here-

## Using KML Bounds

-Insert text here-

## KML Bounds Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Jared Darr](honestcraft.co)
