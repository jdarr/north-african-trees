<?php
/**
 * KML Bounds plugin for Craft CMS 3.x
 *
 * Takes a KML Asset and parses the cordinates to calculate the bounds of the file
 *
 * @link      honestcraft.co
 * @copyright Copyright (c) 2018 Jared Darr
 */

/**
 * KML Bounds en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('kml-bounds', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Jared Darr
 * @package   KmlBounds
 * @since     1.0.0
 */
return [
    'KML Bounds plugin loaded' => 'KML Bounds plugin loaded',
];
