<?php
/**
 * KML Bounds plugin for Craft CMS 3.x
 *
 * Takes a KML Asset and parses the cordinates to calculate the bounds of the file
 *
 * @link      honestcraft.co
 * @copyright Copyright (c) 2018 Jared Darr
 */

namespace honestcraft\kmlbounds;


use Craft;
use craft\base\Plugin;
use craft\base\Element;
use craft\services\Elements;
use craft\events\PluginEvent;

use yii\base\Event;

use Geokit\LatLng;
use Geokit\Bounds;


/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Jared Darr
 * @package   KmlBounds
 * @since     1.0.0
 *
 */
class KmlBounds extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * KmlBounds::$plugin
     *
     * @var KmlBounds
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * To execute your plugin’s migrations, you’ll need to increase its schema version.
     *
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * KmlBounds::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        // Do something after we're installed
        Event::on(
            Elements::class,
            Elements::EVENT_BEFORE_SAVE_ELEMENT,
            function (Event $event) {
                if ($event->element instanceof \craft\elements\Entry) {
                    $element = $event->element;
                    if($element->getType() == 'species') {

                        $assets = $element->getFieldValues(['speciesKml']);
                        if( $assets) {
                            $asset = $assets['speciesKml']->one();
                            if( $asset ) {

                                $xml_str = file_get_contents(CRAFT_BASE_PATH.'/web/uploads/kml/'.$asset->filename);
                                $xml = new \SimpleXMLElement($xml_str);
                                $ns = $xml->getDocNamespaces()[''];
                                $xml->registerXPathNamespace('ns',$ns);
                                $polys = $xml->xpath('///ns:Polygon');

                                $bounds = false;
                                for ($i = 0; $i < sizeof($polys); $i++) {
                                    $coords_str  =  trim($polys[$i]->outerBoundaryIs->LinearRing->coordinates);
                                    $coords = preg_split('/ +/', $coords_str);
                                    foreach($coords as $coord){
                                        $coord_array = explode(',', $coord);
                                        $lng = $coord_array[0];
                                        $lat = $coord_array[1];
                                        $latLng = new LatLng($lat, $lng);
                                        if($bounds != false){
                                            $bounds = $bounds->extend($latLng);
                                        }else{
                                            $bounds = new Bounds($latLng, $latLng);
                                        }
                                    }
                                }

                                //set bounds if needed
                                if( $bounds != false) {
                                    $element->setFieldValues([
                                        'boundsNorth' => $bounds->getNorthEast()->getLatitude(),
                                        'boundsEast' => $bounds->getNorthEast()->getLongitude(),
                                        'boundsSouth' => $bounds->getSouthWest()->getLatitude(),
                                        'boundsWest' => $bounds->getSouthWest()->getLongitude()
                                    ]);
                                }


                            }else{
                                Craft::info(
                                    Craft::t(
                                        'kml-bounds',
                                        'No Asset to parse'
                                    ),
                                    __METHOD__
                                );
                            }
                        }else{
                            Craft::info(
                                Craft::t(
                                    'kml-bounds',
                                    'No Asset Field on Entry'
                                ),
                                __METHOD__
                            );
                        }

                    }
                }
            }
        );

/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        Craft::info(
            Craft::t(
                'kml-bounds',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
