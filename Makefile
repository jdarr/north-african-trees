install:
	docker-compose run --rm frontend-builder npm i

bash:
	docker-compose run --rm frontend-builder bash

dev:
	docker-compose up frontend

api:
	docker-compose up -d app

build:
	docker-compose run --rm frontend-builder npm run build

lint:
	docker-compose run --rm frontend-builder npm run lint

lint-test:
	docker-compose run --rm frontend-builder npm run lint:test

hooks:
	git config --global core.hooksPath .githooks